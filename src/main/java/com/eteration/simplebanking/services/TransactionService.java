package com.eteration.simplebanking.services;

import com.eteration.simplebanking.model.Transaction;
import com.eteration.simplebanking.model.dto.TransactionDto;
import com.eteration.simplebanking.model.dto.TransactionSaveDto;

import java.util.List;

public interface TransactionService {
    public List<TransactionDto> getTransactions(Long accountId);
    public String saveTransaction(TransactionSaveDto transactionSaveDto);

    // Model Mapper
    public TransactionDto EntityToDto(Transaction transaction);
    public Transaction DtoToEntity(TransactionDto transactionDto);
}
