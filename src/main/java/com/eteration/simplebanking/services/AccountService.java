package com.eteration.simplebanking.services;

import com.eteration.simplebanking.model.dto.*;
import com.eteration.simplebanking.model.Account;
import com.eteration.simplebanking.model.Transaction;

public interface AccountService {
    // CRUD
    public AccountDto createAccount(AccountRequestDto accountRequestDto);
    public String post(Transaction transaction) throws Throwable;
    public AccountDetailDto getAccount(String accountNumber);

    // Model Mapper
    public AccountDto EntityToDto(Account account);
    public AccountDetailDto EntityToAccountDetailDto(Account account);
    public Account DtoToEntity(AccountDto accountDto);
    public Account AccountDetailDtoToEntity(AccountDetailDto accountDetailDto);
}
