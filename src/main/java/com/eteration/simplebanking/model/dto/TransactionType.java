package com.eteration.simplebanking.model.dto;

public enum TransactionType {
    DepositTransaction,
    WithdrawalTransaction
}
