package com.eteration.simplebanking.model;

import com.eteration.simplebanking.model.dto.TransactionType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;

// DTO: Data Transfer Object
@Data
@Log4j2
public class WithdrawalTransaction extends Transaction{
    private String accountNumber;
    private double amount;
    private TransactionType type;

    public WithdrawalTransaction(String accountNumber, double amount) {
        this.accountNumber = accountNumber;
        this.amount = amount;
        this.type = TransactionType.WithdrawalTransaction;
    }
}
